package com.example.login;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;

public class Bottomnavigation extends AppCompatActivity {

    TextView mItemSelected;
    String[] listItem;
    boolean[] checkedItems;
    ArrayList<Integer> mUseritems = new ArrayList<>();
    ListView myListView;
    Spinner mySpin;
    ArrayAdapter<CosmicBody> adapter;

    String[] categories = {"All", "Planet", "Star", "Galaxy"};


    private void initializeViews(){
        mySpin = findViewById(R.id.mySpinner);
         mySpin.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, categories));

         myListView = findViewById(R.id.myList);
         myListView.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, getCosmicBodies()));

         mySpin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
             @Override
             public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                 if(position >=0 && position < categories.length){
                     getSelectedCategoryData(position);
                 }
                 else{
                     Toast.makeText(Bottomnavigation.this, "Selected Category Does not Exist!", Toast.LENGTH_SHORT).show();
                 }
             }

             public void onNothingSelected(AdapterView<?>adapterView){

             }
         });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bottomnavigation);
        initializeViews();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Home");
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigator);
        bottomNavigationView.setSelectedItemId(R.id.home);

        mItemSelected = findViewById(R.id.homean);
        listItem = getResources().getStringArray(R.array.category_point);
        checkedItems = new boolean[listItem.length];


        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.calendar:
                        startActivity(new Intent(getApplicationContext(),
                                Calendars.class));
                        overridePendingTransition(0, 0);
                        finish();
                        return true;
                    case R.id.home:
                        return true;
                    case R.id.profile:
                        startActivity(new Intent(getApplicationContext(),
                                Profile.class));
                        overridePendingTransition(0, 0);
                        finish();
                        return true;
                }
                return false;
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.optionsmenu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.item2){

            Intent intent = new Intent(Bottomnavigation.this,infoapplication.class);
            startActivity(intent);
            return true;
        }
        else if(id == R.id.item4){
            Intent intent = new Intent(Bottomnavigation.this, aboutus.class);
            startActivity(intent);
            return true;
        }

        else if(id == R.id.item3) {
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(Bottomnavigation.this);
            mBuilder.setTitle("Category Job").setMultiChoiceItems(listItem, checkedItems, new DialogInterface.OnMultiChoiceClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int position, boolean isChecked) {
                    if (isChecked) {
                            mUseritems.add(position);
                    }
                    else if(mUseritems.contains(position)) {
                            mUseritems.remove(Integer.valueOf(position));
                        }
                    }
            });


            mBuilder.setCancelable(false);
            mBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int i) {
                    StringBuilder item = new StringBuilder();

                    for (i = 0 ; i < mUseritems.size(); i++){
                        item.append(listItem[mUseritems.get(i)]);
                        if(i != mUseritems.size() -1){
                            item.append(", ");
                        }
                    }
                    mItemSelected.setText(item.toString());
                }
            });
            mBuilder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int i) {
                    dialog.dismiss();
                }
            });

            mBuilder.setNeutralButton("Clear All", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int i) {
                    for( i = 0; i < checkedItems.length; i++){
                        checkedItems[i] = false;
                        mUseritems.clear();
                        mItemSelected.setText("");
                    }
                }
            });
            AlertDialog mDialog = mBuilder.create();
            mDialog.show();
        }
        return super.onOptionsItemSelected(item);
    }


    private ArrayList<CosmicBody> getCosmicBodies(){
        ArrayList<CosmicBody> data = new ArrayList<>();
        data.clear();

        data.add(new CosmicBody("Mercury", 1));
        data.add(new CosmicBody("Andromeda", 3));
        data.add(new CosmicBody("Earth", 1));
        data.add(new CosmicBody("Whirlpool", 3));
        data.add(new CosmicBody("Saturn", 2));
        data.add(new CosmicBody("Somb", 2));

        return data;
    }

    private void getSelectedCategoryData(int categoryID){
        ArrayList<CosmicBody> cosmicBodies = new ArrayList<>();

        if(categoryID==0){
            adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, getCosmicBodies());
        }
        else{
            for(CosmicBody cosmicBody : getCosmicBodies()){
                if(cosmicBody.getCategoryID() == categoryID){
                    cosmicBodies.add(cosmicBody);
                }
            }

            adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, cosmicBodies);
        }
        myListView.setAdapter(adapter);
    }
}


class CosmicBody{
    private String name;
    private int categoryID;

    public String getName(){
        return name;
    }

    public int getCategoryID(){
        return categoryID;
    }

    public CosmicBody(String name, int categoryID){
        this.name = name;
        this.categoryID = categoryID;
    }

    @Override
    public String toString(){
        return name;
    }
}
