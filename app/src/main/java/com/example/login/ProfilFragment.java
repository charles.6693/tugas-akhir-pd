package com.example.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.firebase.auth.FirebaseAuth;

public class ProfilFragment extends Fragment {

    private FirebaseAuth firebaseAuth;
    private Button btnTEST;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_notifications, container, false);

        firebaseAuth = FirebaseAuth.getInstance();

        btnTEST = (Button) root.findViewById(R.id.Out);

        btnTEST.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Logout();
                Toast.makeText(getActivity(), " ",Toast.LENGTH_SHORT).show();
            }
        });


        return root;
    }

    private void Logout(){
        firebaseAuth.signOut();
        startActivity(new Intent(getActivity(), MainActivity.class));
    }


}


