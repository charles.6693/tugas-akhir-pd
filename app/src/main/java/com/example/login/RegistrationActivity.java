package com.example.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class RegistrationActivity extends AppCompatActivity {

    private TextView tvmain;
    private EditText userPassword, userEmail,userFirst,userLast,userId;
    private Button regButton;
    private ImageView backBtn;
    private FirebaseAuth firebaseAuth;
    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        setupUIViews();

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        progressDialog = new ProgressDialog(this);
        tvmain = findViewById(R.id.textView9);
        firebaseAuth = FirebaseAuth.getInstance();

        regButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(userFirst.getText().toString().isEmpty() && userLast.getText().toString().isEmpty() && userId.getText().toString().isEmpty()){
                    Toast.makeText(RegistrationActivity.this, "Please Enter All The Details", Toast.LENGTH_SHORT).show();
                }
                else if(validate()){
                    String user_email = userEmail.getText().toString().trim();
                    String user_password = userPassword.getText().toString().trim();

                    progressDialog.setMessage("Loading... Please Wait");
                    progressDialog.show();

                    firebaseAuth.createUserWithEmailAndPassword(user_email, user_password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {

                            if(task.isSuccessful()){
                                progressDialog.dismiss();
                                Toast.makeText(RegistrationActivity.this, "Registration Successful", Toast.LENGTH_SHORT).show();
                                Toast.makeText(RegistrationActivity.this, "Welcome To JobDesk ", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(RegistrationActivity.this, Bottomnavigation.class));
                                finish();
                            }
                            else{
                                Toast.makeText(RegistrationActivity.this, "Registration Failed", Toast.LENGTH_SHORT).show();
                                progressDialog.dismiss();
                            }
                        }
                    });
                }
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setupUIViews();
                finish();
            }
        });

        tvmain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegistrationActivity.this, MainActivity.class));
                finish();
            }
        });


    }

    private void setupUIViews(){
        userFirst = (EditText)findViewById(R.id.First);
        userLast = (EditText)findViewById(R.id.Last);
        userId = (EditText)findViewById(R.id.Id);
        userPassword = (EditText)findViewById(R.id.etUserPassword3);
        userEmail = (EditText)findViewById(R.id.etUserEmail3);
        regButton = (Button)findViewById(R.id.btnRegister);
        backBtn = (ImageView)findViewById(R.id.backbutton);

    }

    private Boolean validate(){
        Boolean result = false;

        String first = userFirst.getText().toString();
        String last = userLast.getText().toString();
        String password = userPassword.getText().toString();
        String email = userEmail.getText().toString();
        String userid = userId.getText().toString();

        if(first.isEmpty() && last.isEmpty() && userid.isEmpty() && password.isEmpty() && email.isEmpty()){
            Toast.makeText(this, "Please enter all the details", Toast.LENGTH_SHORT).show();
        }
        else if(last.isEmpty() && userid.isEmpty() && password.isEmpty() && email.isEmpty()){
            Toast.makeText(this, "Please enter all the details", Toast.LENGTH_SHORT).show();
        }
        else if(userid.isEmpty() && password.isEmpty() && email.isEmpty()){
            Toast.makeText(this, "Please enter all the details", Toast.LENGTH_SHORT).show();
        }
        else if(password.isEmpty() && email.isEmpty()){
            Toast.makeText(this, "Please enter all the details", Toast.LENGTH_SHORT).show();
        }
        else if(email.isEmpty()){
            Toast.makeText(this, "Please enter all the details", Toast.LENGTH_SHORT).show();
        }
        else if(password.isEmpty()){
            Toast.makeText(this, "Please enter all the details", Toast.LENGTH_SHORT).show();
        }
        else{
            result = true;
        }

        return result;
    }
}
