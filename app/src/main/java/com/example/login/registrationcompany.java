package com.example.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class registrationcompany extends AppCompatActivity {

    private EditText userPassword, userEmail,userFirst,userId;
    private Button regButton;
    private FirebaseAuth firebaseAuth;
    private ImageView backs;
    private ProgressDialog progressDialog;
    private TextView tvmain;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrationcompany);
        setupUIViews();

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        progressDialog = new ProgressDialog(this);
        tvmain = findViewById(R.id.textView9);


        firebaseAuth = FirebaseAuth.getInstance();

        regButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(userFirst.getText().toString().isEmpty() && userId.getText().toString().isEmpty()){
                    Toast.makeText(registrationcompany.this, "Please Enter All The Details", Toast.LENGTH_SHORT).show();
                }
                else if(validate()){
                    String user_email = userEmail.getText().toString().trim();
                    String user_password = userPassword.getText().toString().trim();

                    progressDialog.setMessage("Loading... Please Wait");
                    progressDialog.show();

                    firebaseAuth.createUserWithEmailAndPassword(user_email, user_password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {

                            if(task.isSuccessful()){
                                progressDialog.dismiss();
                                Toast.makeText(registrationcompany.this, "Registration Successful", Toast.LENGTH_SHORT).show();
                                Toast.makeText(registrationcompany.this, "Welcome To JobDesk", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(registrationcompany.this, Bottomnavigation.class));
                                finish();
                            }
                            else{
                                Toast.makeText(registrationcompany.this, "Registration Failed", Toast.LENGTH_SHORT).show();
                                progressDialog.dismiss();
                            }
                        }
                    });
                }
            }
        });
        backs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setupUIViews();
                finish();
            }
        });

        tvmain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(registrationcompany.this, MainActivity.class));
                finish();
            }
        });


    }

    private void setupUIViews(){
        userFirst = (EditText)findViewById(R.id.first);
        userId = (EditText)findViewById(R.id.id);
        userEmail = (EditText)findViewById(R.id.userEmail1);
        userPassword = (EditText)findViewById(R.id.userPassword1);
        regButton = (Button)findViewById(R.id.btnRegister3);
        backs = (ImageView)findViewById(R.id.backbutton);
    }

    private Boolean validate(){
        Boolean result = false;

        String ai = userFirst.getText().toString();
        String ui = userId.getText().toString();
        String pi = userPassword.getText().toString();
        String ei = userEmail.getText().toString();

        if(ai.isEmpty() && ui.isEmpty() && pi.isEmpty() && ei.isEmpty()){
            Toast.makeText(this, "Please Enter All The Details", Toast.LENGTH_SHORT).show();
        }
        else if(ui.isEmpty() && pi.isEmpty() && ei.isEmpty()){
            Toast.makeText(this, "Please Enter All The Details", Toast.LENGTH_SHORT).show();
        }
        else if(pi.isEmpty() && ei.isEmpty()){
            Toast.makeText(this, "Please Enter All The Details", Toast.LENGTH_SHORT).show();
        }
        else if(ei.isEmpty()){
            Toast.makeText(this, "Please Enter All The Details", Toast.LENGTH_SHORT).show();
        }
        else if(pi.isEmpty()){
            Toast.makeText(this, "Please Enter All The Details", Toast.LENGTH_SHORT).show();
        }
        else{
            result = true;
        }

        return result;
    }
}
